package com.solr.demo2.service;

import org.apache.solr.common.SolrDocumentList;

import java.util.List;
import java.util.Map;

/**
 * Created by hoangvietanh on 9/22/16.
 */
public interface SolrService {
    void setQuery(String key, String value);

    void setQuery(Map<String, String> queryMap);

    SolrDocumentList getDocuments();

    List<String> getResultsPath(SolrDocumentList solrDocuments);

    Map<String, String> getResultPathAndHighLight();

    void indexFile(String filePath);

    void deleteAll();

}
