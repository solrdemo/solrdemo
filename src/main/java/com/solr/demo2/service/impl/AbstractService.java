package com.solr.demo2.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by hoangvietanh on 9/22/16.
 */
public abstract class AbstractService {
    protected static final Logger LOGGER = LoggerFactory.getLogger(AbstractService.class);
}
