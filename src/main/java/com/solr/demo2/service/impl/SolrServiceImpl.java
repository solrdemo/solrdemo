package com.solr.demo2.service.impl;

import com.solr.demo2.service.SolrService;
import com.solr.demo2.solr.Solr;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class SolrServiceImpl extends AbstractService implements SolrService {
    private final Solr solr;
    private static final String SOLR_DOCUMENT_KEY_ABSOLUTE_PATH = "absolute_path";

    @Autowired
    public SolrServiceImpl(Solr solr) {
        this.solr = solr;
    }

    @Override
    public void setQuery(String key, String value) {
        LOGGER.debug(String.format("SolrService setQuery key=? value=?", key, value));
        solr.setQuery("hl", "on");
        solr.setQuery("hl.fl", "content");
        solr.setQuery("hl.simple.pre", "<mark>");
        solr.setQuery("hl.simple.post", "</mark>");
        solr.setQuery(key, value);
    }

    @Override
    public void setQuery(Map<String, String> queryMap) {
        LOGGER.debug("SolrService serQuery Map");
        solr.setQuery(queryMap);
    }

    @Override
    public SolrDocumentList getDocuments() {
        LOGGER.debug("SolrService getDocuments");
        return solr.getSolrDocumentList(solr.execute());
    }

    @Override
    public List<String> getResultsPath(SolrDocumentList solrDocuments) {
        if (solrDocuments != null) {
            return solrDocuments.stream().map(solrDocument -> solrDocument.get(SOLR_DOCUMENT_KEY_ABSOLUTE_PATH).toString()).collect(Collectors.toList());
        } else return null;
    }

    @Override
    public Map<String, String> getResultPathAndHighLight() {
        QueryResponse response = solr.execute();
        if (response == null) {
            return new HashMap<>();
        }
        Map<String, Map<String, List<String>>> highlight     = response.getHighlighting();
        SolrDocumentList                       solrDocuments = response.getResults();
        Map<String, String>                    retVals       = new HashMap<>();
        if (!solrDocuments.isEmpty()) {
            for (SolrDocument document : solrDocuments) {
                if (highlight.get(document.get("id")).containsKey("content")) {
                    String key = document.getFieldValue(SOLR_DOCUMENT_KEY_ABSOLUTE_PATH).toString();
                    key = key.substring(1, key.length() - 1);
                    retVals.put(key, highlight.get(document.get("id")).get("content").get(0));
                }
            }
        }
        return retVals;
    }

    @Override
    public void indexFile(String filePath) {
        solr.indexFile(filePath);
    }

    @Override
    public void deleteAll() {
        try {
            solr.deleteAll();
        } catch (IOException | SolrServerException e) {
            e.printStackTrace();
        }
    }
}
