package com.solr.demo2.solr;

import org.apache.http.entity.ContentType;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.request.ContentStreamUpdateRequest;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created on 9/11/16.
 * Solr Singleton Entity
 */
@Repository
public class Solr {
    private static final org.slf4j.Logger LOGGER     = LoggerFactory.getLogger(Solr.class);
    private static final String           urlString  = "http://192.168.130.120:8983/solr/demo2";
    private static       SolrQuery        solrQuery  = new SolrQuery();
    private static final SolrClient       solrClient = new HttpSolrClient.Builder(urlString).build();

    public void setQuery(String key, String value) {
        solrQuery.set(key, value);
    }

    public void setQuery(Map<String, String> queryStringMap) {
        for (String key : queryStringMap.keySet()) {
            setQuery(key, queryStringMap.get(key));
        }
    }

    public void setQueryString(String queryString) {
        solrQuery.setQuery(queryString);
    }

    /**
     * Indexing single file
     *
     * @param filePath path of file to be indexed
     * @return true if file is indexed successfully, false if not
     */
    public boolean indexFile(String filePath) {
        LOGGER.debug(">>>> indexing file: " + filePath);
        File    file   = new File(filePath);
        boolean retVal = false;

        if (file.exists() && file.isFile()) {
            ContentStreamUpdateRequest request = new ContentStreamUpdateRequest("/update/extract");
            try {
                request.addFile(file, ContentType.DEFAULT_BINARY.toString());
                request.setParam("literal.absolute_path", file.getAbsolutePath());
                solrClient.request(request);
                solrClient.commit();
                retVal = true;
            } catch (IOException | SolrServerException e) {
                e.printStackTrace();
                retVal = false;
            }
        }
        return retVal;
    }

    /**
     * Find and indexing all files in dirPath
     *
     * @param dirPath indexing directory
     * @return list of files cannot be indexed
     */
    public List<String> indexDirectory(String dirPath) {
        LOGGER.debug(">>>> indexing directory: " + dirPath);
        File         directory  = new File(dirPath);
        List<String> errorFiles = new ArrayList<>();

        // check if directory exists
        if (directory.exists() && directory.isDirectory()) {
            // get all files in directory and check if file is directory or not
            File[] listFiles = directory.listFiles();
            if (listFiles != null) {
                for (File file : listFiles) {
                    if (file.isFile()) {
                        if (!indexFile(file.getAbsolutePath())) {
                            errorFiles.add(file.getAbsolutePath());
                        }
                    }
                    if (file.isDirectory()) {
                        LOGGER.debug(">>>> indexing sub directory: " + dirPath);
                        indexDirectory(file.getAbsolutePath());
                    }
                }
            }
        }
        return errorFiles;
    }

    /**
     * Delete all document indexed by Solr
     *
     * @throws IOException         if file cannot be found
     * @throws SolrServerException if com.solr.demo.solr server cannot be found
     */
    public void deleteAll() throws IOException, SolrServerException {
        solrClient.deleteByQuery("*:*");
        solrClient.commit();
    }

    /**
     * Get result document from query
     *
     * @param queryResponse result of the query
     * @return Solr Result Document List
     */
    public SolrDocumentList getSolrDocumentList(QueryResponse queryResponse) {
        if (queryResponse != null) {
            return queryResponse.getResults();
        } else return null;
    }

    /**
     * Get documents by ids
     *
     * @param docIds list of document id
     * @return Solr Result Document List
     */
    public SolrDocumentList getSolrDocumentList(List<String> docIds) {
        SolrDocumentList solrDocuments = null;
        try {
            solrDocuments = solrClient.getById(docIds);
        } catch (SolrServerException | IOException e) {
            e.printStackTrace();
        }
        return solrDocuments;
    }

    /**
     * Get document by id
     *
     * @param docId document id
     * @return Solr Document
     */
    public SolrDocument getDocumentById(String docId) {
        SolrDocument solrDocument = null;
        try {
            solrDocument = solrClient.getById(docId);
        } catch (SolrServerException | IOException e) {
            e.printStackTrace();
        }
        return solrDocument;
    }

    public Map<String, Map<String, List<String>>> getHighlight(QueryResponse queryResponse) {
        if (queryResponse != null) {
            return queryResponse.getHighlighting();
        }
        return null;
    }

    public QueryResponse execute() {
        QueryResponse response = null;
        try {
            response = solrClient.query(solrQuery);
        } catch (SolrServerException | IOException e) {
            LOGGER.error("Error Querying Solr: " + e.getMessage());
        }
        return response;
    }


}
