package com.solr.demo2.config;

import com.solr.demo2.storage.StorageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.Environment;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;

import javax.servlet.MultipartConfigElement;

/**
 * Created by hoangvietanh on 9/22/16.
 */
@Configuration
@ConfigurationProperties("solr")
@Import(StorageProperties.class)
public class ApplicationConfig {
    private final Environment env;

    @Autowired
    public ApplicationConfig(Environment env) {
        this.env = env;
    }

    @Bean
    public MultipartResolver multipartResolver() {
        return new StandardServletMultipartResolver();
    }

    @Bean
    MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        factory.setMaxFileSize(env.getRequiredProperty("multipart.MAX.FILE.SIZE"));
        factory.setMaxRequestSize(env.getRequiredProperty("multipart.MAX.FILE.SIZE"));

        return factory.createMultipartConfig();
    }
}
