package com.solr.demo2.domain;

import org.apache.solr.client.solrj.beans.Field;

public class BaseDocument {

    @Field("absolute_path")
    private String absolutePath;

    @Field("author")
    private String author;

    @Field("title")
    private String title;

    @Field("content")
    private String content;

    public String getAbsolutePath() {
        return absolutePath;
    }

    public void setAbsolutePath(String absolutePath) {
        this.absolutePath = absolutePath;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
