package com.solr.demo2.controller;

import com.solr.demo2.service.SolrService;
import com.solr.demo2.storage.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by hoangvietanh on 9/20/16.
 */
@Controller
public class SearchController {
    private       StorageService storageService;
    private final SolrService    solrService;

    @Autowired
    public SearchController(StorageService storageService, SolrService solrService) {
        this.storageService = storageService;
        this.solrService = solrService;
    }


    @GetMapping("/greeting")
    public String greeting(@RequestParam(value = "name", required = false, defaultValue = "World") String name, Model model) {
        model.addAttribute("name", name);
        return "greeting";
    }

    @GetMapping("/search")
    public String search(Model model) {
        model.addAttribute("query");

        Map<String, String> result = solrService.getResultPathAndHighLight();
        if (!result.isEmpty()) {
            Map<String, String> retVals = new HashMap<>();
            for (String key : result.keySet()) {
                retVals.put(MvcUriComponentsBuilder
                        .fromMethodName(
                                FileUploadController.class,
                                "serveFile",
                                Paths.get(key).getFileName().toString()).build().toString(), result.get(key));
            }
//            Set<String> resources = result.keySet();
//            model.addAttribute("files", resources
//                    .stream()
//                    .map(s -> MvcUriComponentsBuilder
//                            .fromMethodName(
//                                    FileUploadController.class,
//                                    "serveFile",
//                                    Paths.get(s).getFileName().toString())
//                            .build()
//                            .toString())
//                    .collect(Collectors.toList()));
//            model.addAttribute("content", result.values());
            model.addAttribute("files", retVals);
        }

        return "search";
    }


    @PostMapping("/search")
    public String searchSolr(@RequestParam("query") String query) {
        solrService.setQuery("q", query);
        solrService.setQuery("hl", "true");
        return "redirect:/search";
    }

    @GetMapping("/deleteAll")
    public void deleteAll() {
        solrService.deleteAll();
        storageService.deleteAll();
    }
}
